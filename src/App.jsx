import * as React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import { projects as initialProjects } from './data/projects';
import Header from './components/Header';
import ProjectForm from './components/ProjectForm';
import Home from './pages/Home';

function App() {
  const [projects, setProjects] = React.useState(initialProjects);
  const managers = projects.map(({ project_manager: projectManager }) => projectManager);
  const persons = projects.map(({ person_assigned: person }) => person);

  //esta función quedó a modo de ejemplo y finalmente no se usa
  const addProject = (project) => {
    setProjects((previousProjects) => [...previousProjects, project]);
  };

  const deleteProject = (projectId) => {
    const newProjects = projects.filter(({ id }) => id !== projectId);
    setProjects(newProjects);
  };

  return (
    <>
      <Header />
      <Router>
        <Switch>
          <Route exact path='/'>
            <Redirect to='/projects' />
          </Route>
          <Route exact path='/projects'>
            <Home deleteProject={deleteProject} projects={projects} />
          </Route>
          <Route exact path='/add'>
            <ProjectForm addProject={addProject} managers={managers} persons={persons} />
          </Route>
          <Route
            exact
            path='/projects/edit/:projectId'
            render={({ match }) => {
              const { projectId } = match.params;
              const project = projects.find(
                ({ id }) => id === Number.parseInt(projectId)
              );
              return (
                <ProjectForm
                  managers={managers}
                  persons={persons}
                  projectData={project}
                />
              );
            }}
          />
        </Switch>
      </Router>
    </>
  );
}

export default App;
