import * as React from 'react';

import logo from '../assets/images/logo.png';

function Header() {
  return (
    <header>
      <img alt='Logo de Esto Es' className='py-3 ps-4' src={logo} />
    </header>
  );
}

export default Header;
