import * as React from 'react';
import PropTypes from 'prop-types';
import { Field, Form, Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { Form as FormBootstrap, FormLabel, FormControl, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

import { ReactComponent as BackArrow } from '../assets/icons/back_arrow.svg';

function ProjectForm({ projectData, managers, persons }) {
  const { goBack } = useHistory();

  const h1Text = projectData?.id ? 'Edit project' : 'Add project';
  const buttonText = projectData?.id ? 'Save changes' : 'Create project';

  //por ahora solo se muestran los valores ingresados por consola, pero esta variable es la que indicaría qué acción se debe ejecutar el hacer submit: crear un projecto o guardar el que fue editado
  // const formAction = projectData?.id ? 'save' : 'create';

  //este objeto se usaría al realizar el submit y es el que debería contener los métodos para crear y para editar un proyecto
  // const submitActions = {
  //   create: (project) => addProject({ ...project, id: project.name }),
  // };

  const { name, description, manager, assignedTo, personId, managerId, enable } = {
    name: projectData?.project_name || '',
    description: projectData?.description || '',
    manager: projectData?.project_manager.name || '',
    assignedTo: projectData?.person_assigned.name || '',
    personId: projectData?.person_assigned?.id || 'default',
    managerId: projectData?.project_manager?.id || 'default',
    enable: projectData?.enable || 'true',
  };

  const createOption = ({ name, id }) => (
    <option key={id} value={id}>
      {name}
    </option>
  );

  const managersOptions = managers.map(createOption);
  const personsOptions = persons.map(createOption);

  return (
    <main>
      <div className='d-flex align-items-center mb-4'>
        <Button className='ms-3' variant='light' onClick={goBack}>
          <BackArrow className='me-3' />
          <span>Back</span>
        </Button>
        <h1 className='fs-2 ms-3 mb-0'>{h1Text}</h1>
      </div>
      <Formik
        initialValues={{ name, description, manager, assignedTo }}
        validationSchema={Yup.object({
          name: Yup.string().required('Name is required.'),
          description: Yup.string().required('Description is required'),
          manager: Yup.string().required('Manager is required'),
          assignedTo: Yup.string().required('Assigned person is required'),
        })}
        onSubmit={(values) => console.log(values)}
      /*por ahora solo se muestran los valores por consola. El objetivo final es que el on submit sea del siguiente modo:
    onSubmit={(values) => submitActions[formAction](values)}*/
      >
        {({ errors, handleChange }) => (
          <Form className='px-3'>
            <div className='position-relative mb-3'>
              <FormLabel htmlFor='name'>Project name</FormLabel>
              <Field
                as={FormControl}
                className={errors.name && 'is-invalid'}
                id='name'
                name='name'
                type='text'
              />
              <ErrorMessage name='name'>
                {(msg) => <div className='invalid-tooltip mt-1'>{msg}</div>}
              </ErrorMessage>
            </div>
            <div className='position-relative mb-3'>
              <FormLabel htmlFor='description'>Description</FormLabel>
              <Field
                as={FormControl}
                className={errors.description && 'is-invalid'}
                id='description'
                name='description'
                type='text'
              />
              <ErrorMessage name='description'>
                {(msg) => <div className='invalid-tooltip mt-1'>{msg}</div>}
              </ErrorMessage>
            </div>
            <div className='position-relative mb-3'>
              <FormLabel htmlFor='manager'>Project manager</FormLabel>
              <FormBootstrap.Select
                aria-label='Project manager'
                className={errors.manager && 'is-invalid'}
                defaultValue={managerId}
                id='manager'
                onChange={handleChange}
              >
                <option>Select a person</option>
                {managersOptions}
              </FormBootstrap.Select>
              <ErrorMessage name='manager'>
                {(msg) => <div className='invalid-tooltip mt-1'>{msg}</div>}
              </ErrorMessage>
            </div>
            <div className='position-relative mb-3'>
              <FormLabel htmlFor='assignedTo'>Assigned to</FormLabel>
              <FormBootstrap.Select
                aria-label='Assigned to'
                className={errors.assignedTo && 'is-invalid'}
                defaultValue={personId}
                id='assignedTo'
                onChange={handleChange}
              >
                <option value='default'>Select a person</option>
                {personsOptions}
              </FormBootstrap.Select>
              <ErrorMessage name='assignedTo'>
                {(msg) => <div className='invalid-tooltip mt-1'>{msg}</div>}
              </ErrorMessage>
            </div>
            <div className='mb-3'>
              <FormLabel htmlFor='status'>Status</FormLabel>
              <FormBootstrap.Select
                aria-label='Status'
                defaultValue={enable}
                id='status'
                onChange={handleChange}
              >
                <option value={enable}>Enable</option>
                <option value={enable}>Disable</option>
              </FormBootstrap.Select>
            </div>
            <Button className='text-white py-2 px-3' type='submit' variant='danger'>
              {buttonText}
            </Button>
          </Form>
        )}
      </Formik>
    </main>
  );
}

ProjectForm.propTypes = {
  projectData: PropTypes.object,
  managers: PropTypes.array,
  persons: PropTypes.array,
  addProject: PropTypes.func,
};

export default ProjectForm;
