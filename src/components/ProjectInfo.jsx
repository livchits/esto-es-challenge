import * as React from 'react';
import PropTypes from 'prop-types';

import { formatDate } from '../utils/formatDate';

function ProjectInfo({ projectData, children }) {
  const {
    project_name: projectName,
    created_at: createdAt,
    person_assigned: { name, avatar },
  } = projectData;

  return (
    <>
      <section className='d-flex justify-content-between px-3 py-2 border-bottom border-top border-light border-2'>
        <div>
          <h2 className='fs-6 mb-0 lh-base'>{projectName}</h2>
          <p className='text-secondary fs-sm mb-0'>
            Creation date: {formatDate(createdAt)}
          </p>
          <div className='d-flex align-items-center my-1'>
            <img alt='Person avatar' className='rounded-circle w-15' src={avatar} />
            <p className='mb-0 ms-2 fs-sm'>{name}</p>
          </div>
        </div>
        {children}
      </section>
    </>
  );
}

ProjectInfo.propTypes = {
  projectData: PropTypes.object,
  children: PropTypes.element,
};

export default ProjectInfo;
