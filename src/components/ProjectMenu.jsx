import PropTypes from 'prop-types';
import * as React from 'react';
import { OverlayTrigger, Popover } from 'react-bootstrap';
import { Link, useRouteMatch } from 'react-router-dom';

import { ReactComponent as DeleteIcon } from '../assets/icons/delete_icon.svg';
import { ReactComponent as EditIcon } from '../assets/icons/edit_icon.svg';
import { ReactComponent as OpenMenu } from '../assets/icons/open_menu.svg';

function ProjectMenu({ projectId, deleteProject }) {
  const { path } = useRouteMatch();

  return (
    <OverlayTrigger
      overlay={
        <Popover className='top-45 w-150' id='popover-positioned-bottom'>
          <Popover.Body
            as={Link}
            className='py-2 d-flex align-items-center text-decoration-none'
            to={`${path}/edit/${projectId}`}
          >
            <EditIcon className='me-3' />
            <span>Edit</span>
          </Popover.Body>
          <Popover.Body
            className='py-2 d-flex align-items-center pointer'
            onClick={() => deleteProject(projectId)}
          >
            <DeleteIcon className='me-3' />
            Delete
          </Popover.Body>
        </Popover>
      }
      placement='bottom'
      trigger='click'
    >
      <div className='pointer'>
        <OpenMenu />
      </div>
    </OverlayTrigger>
  );
}

ProjectMenu.propTypes = { projectId: PropTypes.number, deleteProject: PropTypes.func };

export default ProjectMenu;
