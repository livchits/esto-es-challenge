import * as React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import ProjectInfo from '../components/ProjectInfo';
import ProjectMenu from '../components/ProjectMenu';

function Home({ projects, deleteProject }) {
  const projectsList = projects.map((project) => {
    return (
      <ProjectInfo key={project.id} projectData={project}>
        <ProjectMenu deleteProject={deleteProject} projectId={project.id} />
      </ProjectInfo>
    );
  });

  return (
    <main>
      <div className='d-flex justify-content-between px-4 mb-3'>
        <h1 className='fs-5 align-self-center lh-1 mb-0'>My projects</h1>
        <Button as={Link} className='py-1' to='/add' variant='danger'>
          <span className='fs-3'>+</span> Add project
        </Button>
      </div>
      {projectsList}
    </main>
  );
}

Home.propTypes = { projects: PropTypes.array, deleteProject: PropTypes.func };

export default Home;
