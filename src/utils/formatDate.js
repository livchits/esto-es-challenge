const options = {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
  dayPeriod: 'short',
};

const dateFormater = new Intl.DateTimeFormat('es-AR', options);

export function formatDate(dateString) {
  const date = new Date(dateString);
  return dateFormater.format(date);
}
