import { defineConfig } from 'vite';
import reactRefresh from '@vitejs/plugin-react-refresh';
import ViteFonts from 'vite-plugin-fonts';
import svgrPlugin from 'vite-plugin-svgr';
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    reactRefresh(),
    ViteFonts({
      // Google Fonts API V2
      google: {
        /**
         * Fonts families lists
         */
        families: [
          {
            /**
             * Family name (required)
             */
            name: 'Roboto',

            /**
             * Family styles
             */
            styles: 'wght@400;500',
          },
        ],
      },
    }),
    svgrPlugin(),
  ],
});
